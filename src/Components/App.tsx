import * as React from 'react'
import Helmet from  'react-helmet'
import { Route, Switch, NavLink } from 'react-router-dom'
import { classes } from '../helpers/style'
import { Home, TodoAPI, TodoLocal } from './Pages'
import { ReactLogo } from './ReactLogo'
import * as s from './style'
import Chat from './Chat'
import Authors from './Authors'

const App = () =>
  <div className={s.main}>
    <Helmet titleTemplate="%s | MyAwesomeWebsite.com">
      <html lang="en"/>
      <title>My App</title>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
    </Helmet>
    <div className={s.header}>
      <div className={s.container}>
        <ReactLogo/>
        <h2>Welcome to Razzle Dazzle</h2>
      </div>
    </div>
    <div className={classes(s.nav, s.container)}>
      <NavLink exact={true} to="/">Home</NavLink>
      <NavLink exact={true} to="/todo-local">Todo (LocalStorage)</NavLink>
      <NavLink exact={true} to="/todo-sql">Todo (SQLite)</NavLink>
      <NavLink exact={true} to="/chat">Chat</NavLink>
      <NavLink exact={true} to="/authors">Library</NavLink>
    </div>
    <div className={s.container}>
      <Switch>
        <Route exact={true} path="/" component={Home} />
        <Route exact={true} path="/todo-local" component={TodoLocal} />
        <Route exact={true} path="/todo-sql" component={TodoAPI} />
        <Route exact={true} path="/chat" component={Chat} />
        <Route exact={true} path="/authors" render={(props) => <Authors baseRoute={props.match.url}/>} />
      </Switch>
    </div>
    <footer className={s.footer}>
      all rights reserved copyright you
    </footer>
  </div>

export default App