import * as React from  'react'
import { style, classes } from '../../helpers/style'

type RawProps = {
  children:any
} & React.HTMLAttributes<HTMLDivElement>

const rawStyle = style({
  textAlign:'left',
  padding:'1em'
})

export const Raw = ({children, className,...rest}:RawProps) =>
  <div className={classes(rawStyle,className)} dangerouslySetInnerHTML={{__html:`${children}`}} {...rest}/>