/**
 * These are functions meant to be used to create starting data
 * to fill the database. They're not for anything else
 * They wouldn't be used in an actual app, they're only used
 * here because this is a demo app
 */
const created_at = new Date().getTime()
const updated_at = created_at

/**
 * Creates a book
 * @param title the title of the book
 * @param author_id the book's author id
 */
const make_book = (title:string, author_id?:number):BookToInsert => {
  return {
    title,
    created_at,
    updated_at,
    author_id
  }
}

/**
 * Creates an author without an id
 * @param name_first first name of the author
 * @param name_family last name of the author
 * @param books books owned by the author
 */
const make_author = (name_first:string, name_family:string, books:string[]):AuthorToInsert => {
  return {
    name_first,
    name_family,
    created_at,
    updated_at,
    books:books.map(title=>make_book(title))
  }
}

export const init_authors:AuthorToInsert[] = [
  make_author('Herman','Melville',['Moby Dick','Omoo']),
  make_author('William','Shakespear',['Hamlet','MacBeth','A Midsummer Night\'s Dream']),
  make_author('Victor','Hugo',['Les Misérables','Toilers of the Sea'])
]