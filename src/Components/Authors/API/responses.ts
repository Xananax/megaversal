import { Response } from 'express'

export const OK = <T>(data:T):ReturnValueOk<T> => {
  return {
    status:'success',
    data
  }
}

export const isOK = (val:any):val is ReturnValueOk<any> => val && val.status && val.status === 'success'
export const isNO = (val:any):val is ReturnValueNo => val && val.status && val.status === 'error'

export const NO = (err?:Error|string):ReturnValueNo => {
  const status = 'error'
  if(err && err instanceof Error){
    const {message, stack} = err;
    return {
      status,
      error: {
        message,
        stack:(stack ? stack.split('\n') : [])
      }
    }
  }else{
    try{
      const message = ( typeof err === 'string' ? err : `unknown error: [${err}]`)
      return {
        status,
        error: {
          message,
          stack:[]
        }
      }
    }catch(e){
      return {
        status,
        error:{
          message:'unknown',
          stack:[]
        }
      }
    }
  }
}