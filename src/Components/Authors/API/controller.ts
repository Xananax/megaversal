import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as path from 'path'
import { Response as Res, Request as Req } from 'express'
import * as m from './model'
import * as multer from 'multer'
import { isOK, isNO } from './responses'

/////////////////////////////////////////
// UTILITY FUNCTIONS
/////////////////////////////////////////

/**
 * Simple wrapper around express' `response.send`
 * that always sends json
 *
 * Returns a function that expects to receive a value such as
 *
 * ```
 * { status:"success",
 *   data: <any>
 * }
 * ```
 * or
 * ```
 * { status:"error"
 * , error:{
 *      message: string
 *   }
 * }
 * ```
 *
 * @param res the response from express
 */
const send = (res:Res) => (answer:ReturnValue<any>) => {
    if(isOK(answer)){
      res.status(200).send(answer)
    }else if(isNO(answer)){
      res.status(500).send(answer)
    }else{
      res.status(500).send({status:'fatal_error',answer})
    }
  }

/////////////////////////////////////////
// ROUTE HANDLERS
/////////////////////////////////////////

const getAuthor = ({params:{id}}:Req, res:Res) => m.get_author(id).then(send(res))

const getAllAuthors = (req:Req, res:Res) => m.list().then(send(res))

const createAuthor = ({query:{name,family}}:Req, res:Res) => m.create_author({name_family:family, name_first:name}).then(send(res))

const createBook = ({params:{id:author_id},file,query:{title}}:Req, res:Res) => {
  const cover = ( file ? file.path : '' )
  return m.create_book({title,author_id,cover}).then(send(res))
}

const deleteAuthor = ({params:{id}}:Req, res:Res) => m.remove_author(id).then(send(res))

const deleteBook = ({params:{id}}:Req, res:Res) => m.remove_book(id).then(send(res))

const updateAuthor = ({params:{id},query,body}:Req, res:Res) => m.update_author({id,...query,...body}).then(send(res))

const updateBook = ({params:{id},file,query,body,method}:Req, res:Res) => {
  const cover = ( file
  ? file.path
  : ''
  )
  return m.update_book({id,cover,...query,...body}).then(send(res))
}

const resetDB = (req:Req, res:Res) => m.resetDatabase().then(send(res))

/////////////////////////////////////////
// SETTING UP ACTUAL ROUTER
/////////////////////////////////////////

// creating an express app
const app = express()

// making the express app capable of receiving
// POST data
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

// making the express app capable of handing file uploads
// this will be used for the book's covers
const uploadDestination = path.join(process.env.RAZZLE_PUBLIC_DIR || '', 'uploads')+'/'
const uploadMiddleWare = multer({dest:uploadDestination})
const uploadField = uploadMiddleWare.single('cover')

app.get('/', (req, res) => {
  res.send({
    status:'success',
    data:{
      endpoints:[
        '/list',
        '/author/create',
        '/author/:id',
        '/author/:id/edit',
        '/author/:id/delete',
        '/author/:id/add',
        '/book/:id/edit',
        '/book/:id/delete'
      ]
    }
  })
})

app.get('/list',getAllAuthors)
app.get('/author/create',createAuthor)
app.get('/author/:id',getAuthor)
app.get('/author/:id/add',createBook)
app.get('/author/:id/edit',updateAuthor)
app.get('/author/:id/delete',deleteAuthor)
app.get('/book/:id/delete',deleteBook)
app.get('/book/:id/edit',updateBook)
app.get('/reset',resetDB)

app.post('/author',createAuthor)
app.delete('/author/:id',deleteAuthor)
app.delete('/book/:id',deleteAuthor)
app.put('/author/:id',updateAuthor)
app.put('/book/:id',updateBook)
// when uploading files, we chain the uploadField
// we created earlier, so it can handle the file upload
// note that we don't have a system to remove an old, unused
// cover when it gets replaced
app.post('/author/:id', uploadField, createBook)
app.post('/book/:id', uploadField, updateBook)

export default app