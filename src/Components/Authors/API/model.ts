import * as Knex from 'knex'
import * as Promise from 'bluebird'
import { OK, NO, isOK, isNO } from './responses'
import { init_authors } from './init_authors'

// creates the database
const knex = Knex({
  client: 'sqlite3',
  connection: {
    filename: './library.sqlite'
  },
  // the below removes sqlite warning in the console
  useNullAsDefault: true
})

/**
 * Checks if the id is a number and if
 * it is valid
 * @param id the id
 */
const isInvalidId = (id?:number):id is undefined => {
  return (typeof id === 'undefined' || id === null || isNaN(id) || id < 0)
}

export const createTables = ():Promise<any> => {
  const { schema } = knex

  return Promise.all([
    schema.createTableIfNotExists('authors', (table) => {
      table.increments('id')
      table.string('name_first')
      table.string('name_family')
      table.timestamps()
    }),

    schema.createTableIfNotExists('books', (table) => {
      table.increments('id')
      table.string('title')
      table.string('cover')
      table.integer('author_id').unsigned()
      table.foreign('author_id').references('authors.id')
      table.timestamps()
    })
  ])
  .catch(err =>{ throw err })
  .then( response => {
    if(isNO(response)){
      throw new Error(response.error.message)
    }
    return true;
  })
}

let tablesAreEnsured:Promise<boolean>
/**
 * Makes sure the tables needed exist
 */
export const ensureTables = () => {

  if(!tablesAreEnsured){

    const { schema } = knex

    tablesAreEnsured = schema.hasTable('authors')
      .then( (exists) => {
        if(!exists){
          return createTables()
        }
        return true
      })
      .then( response => {
        if(isNO(response)){
          throw new Error(response.error.message)
        }
        return true
      })
  }
  return tablesAreEnsured
}

export const create_author = ({name_first, name_family, books}:Partial<Author|AuthorToInsert>):Promise<ReturnValue<Author>> => {
  if(!name_first || !name_family){
    return Promise.resolve(NO('an author needs a first name and a last name'))
  }
  const created_at = new Date().getTime()
  const updated_at = created_at
  const item:AuthorToInsert = {
    name_first,
    name_family,
    created_at,
    updated_at
  }
  return ensureTables()
    .then( () => knex.insert(item).into('authors'))
    .then( ([id]) => ({...item, id}))
    .then( (author:Author) => {
      if(books && books.length){
        const _books = (books as any)
          .filter(Boolean)
          .map((book:Book|Partial<Book>)=>({...book,author_id:author.id, created_at, updated_at}))

        author = {...author,books:_books}

        return knex.batchInsert('books', _books, 1000)
          .then( response => OK(author))
          .catch(NO)
      }
      author = {...author,books:[]}
      return OK(author)
    })
    .catch(NO)
}

export const create_book = ({title, author_id, cover}:Partial<Book>):Promise<ReturnValue<Book>> => {
  if(!title){
    return Promise.resolve(NO(`can't create a book without a title`))
  }
  if(!author_id){
    return Promise.resolve(NO(`can't create a book without an author`))
  }
  const created_at = new Date().getTime()
  const updated_at = created_at
  const item:BookToInsert = {
    title,
    created_at,
    updated_at,
    author_id,
    cover
  }
  return ensureTables()
    .then( () => knex.insert(item).into('books'))
    .then( ([id]) => ({...item, id}))
    .then(OK)
    .catch(NO)
}

export const create_batch = (authors:AuthorToInsert[]) => {
  if(!authors || !authors.length){
    return Promise.resolve(NO(`invalid authors batch`))
  }
  const create_all_authors = () => Promise.map(authors, (author) => create_author(author), {concurrency:1})
  return ensureTables()
  .then( create_all_authors )
  .then( () => OK(authors))
  .catch(NO)
}

export const fillFirstData = () => create_batch(init_authors)

export const get_author = (id:number):Promise<ReturnValue<Author>> => {
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  return ensureTables()
    .then( () => knex.select('*').from('authors').where('id',id))
    .then( (author) =>
        knex.select('title').from('books').where('author_id',id)
          .then((books) => ({...author,books}))
          .catch( e => {throw e})
    )
    .then(OK)
    .catch(NO)
}

/**
 * Returns a list of items
 */
export const list = ():Promise<ReturnValue<Author[]>> => {
  return ensureTables()
    .then( () => knex.select('*').from('authors'))
    .then(OK)
    .catch(NO)
}

export const remove_author_books = (id:number) => {
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  return ensureTables()
    .then( () => knex.delete().from('authors').where('id',id))
    .then( (rows:number) => !!rows )
    .then( (success) => success ? OK(success) : NO(`no rows affected`) )
}

export const remove_author = (id:number):Promise<ReturnValue<boolean>> => {
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  return ensureTables()
    .then( () => knex.delete().from('author').where('id',id))
    .then( (rows:number):Promise<ReturnValue<boolean>> => {
      if(rows <= 0){
        return Promise.resolve(NO(`no rows affected`))
      }
      return remove_author_books(id)
        .then( status => OK(true) )
    })
}

export const remove_book = (id:number):Promise<ReturnValue<boolean>> => {
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  return ensureTables()
    .then( () => knex.delete().from('books').where('id',id))
    .then( (rows:number) => rows ? OK(true) : NO(`no rows affected`))
}

export const update_author = (props:Partial<Author>) => {
  if(typeof props === 'undefined' || props === null){
    return Promise.resolve(NO(`no properties passed`))
  }
  const { name_family, name_first, id } = props
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  const updated_at = new Date().getTime()
  const data:Partial<Author> = { name_family, name_first, updated_at}
  return ensureTables()
    .then( () => knex('authors').update(data).where('id',id))
    .then( (rows:number) => ( rows?OK(rows):NO(`no rows affected`)) )
}

export const update_book = (props:Partial<Book>) => {
  if(typeof props === 'undefined' || props === null){
    return Promise.resolve(NO(`no properties passed`))
  }
  const { author_id ,title, id, cover } = props
  if(isInvalidId(id)){
    return Promise.resolve(NO(`id \`${id}\` is invalid`))
  }
  if(!title){
    return Promise.resolve(NO(`you need a title`))
  }
  const updated_at = new Date().getTime()
  const data:Partial<Book> = { title, author_id, updated_at, cover }
  return ensureTables()
    .then( () => get_author(id))
    .then( (status) => {
      if(isNO(status)){ return NO(`author does not exist`)}
      else{
        return knex('books').update(data).where('id',id)
      }
    })
    .then( (rows:number) => ( rows?OK(rows):NO(`no rows affected`)) )
}

export const resetDatabase = () => {
  const { schema } = knex
  return schema.dropTableIfExists('authors')
    .then(() => schema.dropTableIfExists('books'))
    .then(ensureTables)
    .then(fillFirstData)
    .then(OK)
    .catch(NO)
}