import * as React from 'react'
import { Component } from 'react'
import { Route, Switch, NavLink } from 'react-router-dom'
import Helmet from  'react-helmet'
import { List } from './List'
import { isOK, isNO } from './API/responses'
import { wait } from '../../helpers/wait'
import { AuthorPage } from './Author'

type State = FetchResponse<Author[]>

interface Props{
  baseRoute:string
}

export class App extends Component<Props,State>{
  rootUrl = '/api/authors'
  constructor(props:Props){
    super(props)
    this.state = {
      status:'init'
    }
  }
  getAuthors = (path='/list') => {
    const url = this.rootUrl+path
    this.setState( {status:'loading'}, () => {
      fetch(url)
        .then(wait(2000))
        .then( (res) => res.json() )
        .then( (response:ReturnValue<Author[]>) => {
          const newState:State = ( isNO(response)
          ? {
              status:'error',
              error:response.error
            }
          : {
              status:'success',
              data:response.data
            }
          )
          this.setState(newState)
        })
        .catch( err => {
          const { message, stack } = err
          const newState:State = {
            status:'error',
            error:{
              message,
              stack:stack.split('\n')
            }
          }
          this.setState(newState)
        })
    } )
  }
  getAuthorById = (id:number):Author|undefined => {
    const author = this.state.status === 'success' && this.state.data && this.state.data.find((_author)=>_author.id === id)
    if(author){
      return author
    }
    return
  }
  renderAuthor = (id:number) => {
    const {baseRoute:base} = this.props
    console.log(id)
    const author = this.getAuthorById(id)
    return <AuthorPage id={id} author={author} baseRoute={base}/>
  }
  renderError({message,stack}:ReturnValueNo['error']){
    return (
    <div>
      <div>{message}</div>
    </div>)
  }
  componentDidMount(){
    this.getAuthors()
  }
  render(){
    const {props,state} = this
    const {baseRoute:base} = props
    const { status } = state
    const data =  ( state.status === 'success' ? state.data : null )
    const error = ( state.status === 'error'   ? state.error : null )
    return (
      <div>
        <Helmet>
          <title>Authors</title>
        </Helmet>
        <div>
          {status}
          {( error ? this.renderError(error) : null)}
        </div>
        <div>
          <NavLink exact={true} to={`${base}/`}>Home</NavLink>
        </div>
        <div>
          <Switch>
            <Route exact={true} path={`${base}/`} render={()=><List authors={data} baseRoute={base}/>}/>
            <Route path={`${base}/:id`} render={({match}) => this.renderAuthor(match.params.id)}/>
          </Switch>
        </div>
      </div>
    )
  }
}