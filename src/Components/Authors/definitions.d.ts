interface AuthorItem{
  id?:number
  created_at:number
  updated_at:number
}

interface AuthorToInsert extends AuthorItem{
  name_first:string
  name_family:string
  books?:Partial<Book>[]
}

interface Author extends AuthorToInsert{
  id:number
  books:Book[]
}

interface BookToInsert extends AuthorItem{
  title:string
  cover?:string
  author_id?:number
}

interface Book extends BookToInsert{
  id:number  
}

interface FetchResponseLoading{
  status:'loading'
}

interface FetchResponseInit{
  status:'init'
}

interface ReturnValueOk<T>{
  status:'success'
  data:T
}

interface ReturnValueNo{
  status:'error'
  error:{
    message:string
    stack:string[]
  }
}

type ReturnValue<T> = 
  | ReturnValueOk<T>
  | ReturnValueNo

type FetchResponse<T> = 
  | FetchResponseInit
  | FetchResponseLoading
  | ReturnValueOk<T>
  | ReturnValueNo