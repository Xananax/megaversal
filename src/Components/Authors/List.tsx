import * as React from 'react'
import { AuthorSummary } from './Author'

interface Props{
  authors:Author[]|null
  baseRoute?:string
}

export const List = ({authors,baseRoute}:Props) => {
  if(!authors || !authors.length){
    return <div>no authors</div>
  }
  return (
  <div>
    {authors.map( (author) => <AuthorSummary {...author} baseRoute={baseRoute||''} key={author.id}/>)}
  </div>)
}