import * as React from 'react'
import { NavLink } from 'react-router-dom'

export const AuthorNotFound = ({id}:{id:number|string}) => <div>Author {id} not found</div>

export const Author = ({id,name_family,name_first,books,baseRoute}:Author & {baseRoute:string}) =>
  (<div>
    {name_family}
  </div>)

export const AuthorSummary = ({id,name_family,name_first,books,baseRoute}:Author & {baseRoute:string}) =>
  (<div>
    <div>{name_first} {name_family}</div>
    <div>{(books && books.length) || 0}</div>
    <NavLink exact={true} to={`${baseRoute}/${id}`}>view</NavLink>
    <NavLink exact={true} to={`${baseRoute}/${id}/add`}>add books</NavLink>
  </div>)

interface Props{
  id:number|string
  author?:Author
  baseRoute?:string
}

export const AuthorPage = ({id,author,baseRoute}:Props) => {
  if(author){
    return <Author {...author} baseRoute={baseRoute || ''}/>
  }
  return <AuthorNotFound id={id}/>
}