import * as React from 'react'
import { Component } from 'react'
import { TodoStore, Todo, TODOS_TYPES } from './types'
import { Footer } from './Footer'
import { Item } from './Item'
import { ENTER_KEY, ESCAPE_KEY } from './constants'
import './todo.css'

interface Props {
  store: TodoStore
}

interface State {
  editing?: string
  nowShowing: TODOS_TYPES,
  status: 'loading'|'ready'
  todos: Todo[]
}

export class App extends Component<Props, State> {

  newField:HTMLInputElement

  constructor(props: Props) {
    super(props)
    this.state = {
      nowShowing: 'all',
      editing: undefined,
      status:'loading',
      todos: []
    }
  }

  load = () => {
    const setFirstState = (state:State):State => ({...state, status:'loading'})
    const then = () => {
      this.props.store
        .get(this.state.nowShowing)
        .then(
          (todos) => this.setState({todos, status:'ready'})
        )
    }
    this.setState(setFirstState, then)
  }

  componentDidMount(){
    this.load()
  }

  componentDidUpdate(props:Props){
    if(props.store !== this.props.store){
      this.load()
    }
  }

  getRef = (newField:HTMLInputElement) => this.newField = newField

  setNowShowing = (nowShowing: TODOS_TYPES) => {
    if(nowShowing !== this.state.nowShowing){
      this.setState({nowShowing}, this.load)
    }
  }

  handleNewTodoKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode !== ENTER_KEY) {
      return
    }
    event.preventDefault()
    const val = this.newField && this.newField.value.trim()
    if (val) {
      this.newField.value = ''
      this.props.store.add(val).then(this.load)
    }
  }

  toggleAll = (event: React.FormEvent<HTMLInputElement>) => {
    var target: any = event.target
    var checked = target.checked
    this.props.store.toggleAll(checked)
  }

  toggle = (id:string) => this.props.store.toggle(id).then(this.load)

  destroy = (id: string) => this.props.store.destroy(id).then(this.load)

  edit = (id:string) => this.setState({editing: id})

  update = (id: string, text: string) => {
    this.props.store.update(id, text).then(this.load)
    this.setState({editing: undefined})
  }

  cancel = () => this.setState({editing: undefined})

  clearCompleted = () =>  this.props.store.clearCompleted().then(this.load)

  renderItems(todos:Todo[]){

    return todos.map(({id, text, completed}) => {
      const props = {
        id,
        text,
        completed,
        editing: id === this.state.editing,
        onToggle:() => this.toggle(id),
        onDestroy:() => this.destroy(id),
        onEdit:() => this.edit(id),
        onUpdate:(newText:string) => this.update(id, newText),
        onCancel: this.cancel
      }
      return (
        <Item key={id} {...props} />
      )
    })
  }

  render() {

    const { store } = this.props
    const { todos } = this.state
    const items = this.renderItems(todos)

    const footer = (store.activeCount || store.completedCount
    ?  <Footer count={store.activeCount} completedCount={store.completedCount} setNowShowing={this.setNowShowing} nowShowing={this.state.nowShowing} onClearCompleted={this.clearCompleted} />
    : undefined
    )

    const main = (todos.length
    ? (<section className="main">
        <input
          className="toggle-all"
          type="checkbox"
          onChange={this.toggleAll}
          checked={store.activeCount === 0}
        />
        <ul className="todo-list">
          {items}
        </ul>
      </section>)
    : null
    )

    return (
      <div className="todos-body">
        <header className="header">
          <h1>todos</h1>
          <input
            ref={this.getRef}
            className="new-todo"
            placeholder="What needs to be done?"
            onKeyDown={this.handleNewTodoKeyDown}
            autoFocus={true}
          />
        </header>
        {main}
        {footer}
      </div>
    )
  }
}