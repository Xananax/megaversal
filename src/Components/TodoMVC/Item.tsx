import * as React from 'react'
import { Component } from 'react'
import { ENTER_KEY, ESCAPE_KEY } from './constants'
import { classes } from '../../helpers/style'

interface Props{
  key: string
  id: string
  text: string
  completed?: boolean
  editing?: boolean
  onUpdate: (val: string) => void
  onDestroy: () => void
  onEdit: ()  => void
  onCancel: (event: any) => void
  onToggle: () => void
}

interface State {
  editText: string
}

export class Item extends Component<Props, State> {

  state: State
  editField: HTMLInputElement

  constructor(props: Props){
    super(props)
    this.state = { editText: this.props.text }
  }

  handleEdit = () => {
    this.props.onEdit()
    this.setState({editText: this.props.text})
  }

  getRef = (editField:HTMLInputElement) => this.editField = editField

  handleSubmit = () => {
    var val = this.state.editText.trim()
    if (val) {
      this.props.onUpdate(val)
      this.setState({editText: val})
    } else {
      this.props.onDestroy()
    }
  }

  handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode === ESCAPE_KEY) {
      this.setState({editText: this.props.text})
      this.props.onCancel(event)
    } else if (event.keyCode === ENTER_KEY) {
      this.handleSubmit()
    }
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    var input: any = event.target
    this.setState({ editText : input.value })
  }

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    return (
      nextProps.text !== this.props.text ||
      nextProps.completed !== this.props.completed ||
      nextProps.editing !== this.props.editing ||
      nextState.editText !== this.state.editText
    )
  }

  componentDidUpdate(prevProps: Props) {
    if (!prevProps.editing && this.props.editing) {
      const node = this.editField
      if(node){
        node.focus()
        node.setSelectionRange(node.value.length, node.value.length)
      }
    }
  }

  render() {
    const { completed, editing, onToggle, text } = this.props
    const { handleEdit, getRef, handleSubmit, handleChange, handleKeyDown } = this
    const { editText } = this.state
    const className = classes(completed && 'completed', editing && 'editing')
    return (
      <li className={className}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={completed}
            onChange={onToggle}
          />
          <label onDoubleClick={handleEdit}>
            {text}
          </label>
          <button className="todo-button destroy" onClick={this.props.onDestroy} />
        </div>
        <input
          ref={getRef}
          className="edit"
          value={editText}
          onBlur={handleSubmit}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
        />
      </li>
    )
  }
}