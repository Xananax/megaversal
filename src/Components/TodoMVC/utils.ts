
export const uuid = () => ( new Array(12)
  .fill('')
  .map(( _, i) => Math.random() )
  .join('')
  .replace(/\./, '')
  + new Date().getTime()
)

export const pluralize = (count: number, word: string) => count === 1 ? word : word + 's'