export type ALL_TODOS = 'all'
export type ACTIVE_TODOS = 'active'
export type COMPLETED_TODOS = 'completed'
export type TODOS_TYPES =
  | ALL_TODOS
  | ACTIVE_TODOS
  | COMPLETED_TODOS

export interface TodoStore{
  add: (title: string) => Promise<Todo>
  toggleAll: (completed:boolean) => Promise<any>
  toggle: (id:string) => Promise<any>
  destroy: (id:string) => Promise<any>
  update: (id:string, text:string) => Promise<any>
  clearCompleted: () => Promise<any>
  onChange: (handler:(todos:Todo[]) => any) => any
  completedCount: number
  activeCount: number
  get: (which?:TODOS_TYPES) => Promise<Todo[]>
}

export interface Todo{
  id:string
  text:string
  completed:boolean
}