import * as React from 'react'
import { Component } from  'react'
import { classes } from '../../helpers/style'
import { TODOS_TYPES } from './types'
import { pluralize } from './utils'

interface Props {
  completedCount: number
  onClearCompleted: () => any
  setNowShowing: (nowShowing: TODOS_TYPES) => any
  nowShowing: TODOS_TYPES
  count: number
}

export const Footer = ({completedCount, onClearCompleted, setNowShowing, nowShowing, count }:Props) => {

  const e = (which:TODOS_TYPES) => (evt:React.MouseEvent<HTMLAnchorElement>) => {evt.preventDefault(); setNowShowing(which)}
  const a = (which:TODOS_TYPES) => (
    <li>
      <a href={`#${which}/`} onClick={e(which)} className={classes(nowShowing === which && 'selected')}>{which}</a>
    </li>
  )
  const activeTodoWord = pluralize(count, 'item')
  const clearButton = ( completedCount > 0
  ? (
      <button
        className="clear-completed"
        onClick={onClearCompleted}
      >
        Clear completed
      </button>
    )
  : null
  )
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{count}</strong> {activeTodoWord} left
      </span>
      <ul className="filters">
        {a('all')}
        {' '}
        {a('active')}
        {' '}
        {a('completed')}
      </ul>
      {clearButton}
    </footer>
  )
  }