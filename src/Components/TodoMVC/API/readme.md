# TODO REST + SQL

This is a rest interface using SQL for the Todos.

Just a simple SQL model, and a router to control it.

This is, by no means, a good example of how this should be done. There is a lot of repetition, some pointless separation of concerns (there really isn't a need to separate the router and the SQL queries), some hand-made SQL query concatenation (*shiver*).

However, it is designed to be easy to explain and easy to split apart (e.g, look at one function and understand what it does)