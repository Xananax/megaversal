import * as model from './model'
import * as bodyParser from 'body-parser'
import * as express from 'express'

/********************/
/* HELPER FUNCTIONS */
/********************/

/**
 * Verifies if the current request is an ajax request
 * A request is considered to be ajax if the `xhr` flag
 * is true or if there's an `x` parameter in the URL
 * @param req the request
 */
const isAjaxQuery = (req:express.Request) => {
  if(req.xhr || 'x' in req.query || req.headers.accept.indexOf('json') > -1){
    return true;
  }
  return false
}

/**
 * Takes care of sending back any data.
 * @param req the request
 * @param res the response
 * @param data anything that should be sent back
 */
const sendBack = (req:express.Request, res:express.Response, data?:any) => {
  const vars = ( data
  ? {
      status:'success',
      data
    }
  : { status:'success',
      data:false
    }
  )
  if(isAjaxQuery(req)){
    res.status(200).send(vars)
  }else{
    res.send(`<html><body><pre>${JSON.stringify(vars, null, 2)}</pre></body></html>`)
  }
}

/**
 * Turns a string like `"false"` or `"no"`, or
 * a number like `0` or `"0"` to a native `false`
 * falsy values are also turned into false of course.
 * The rest resolves to true
 * @param completed any value
 */
const completedParamToArgument = (completed?:any) => {
  if(typeof completed === 'string'){
    return ( completed === 'false' || parseFloat(completed) === 0 || completed === 'no'
    ? false
    : true
    )
  }
  return !!completed
}

/**************/
/* ACTUAL API */
/**************/

/**
 * Gets one item
 * Needs an `id` parameter
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const getOne = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const { id } = req.params
  model.getOne(id)
  .then(([todo]) => {
    if(!todo){
      return next({status:404, err:new Error(`todo \`${id}\` not found`)})
    }
    sendBack(req, res, todo)
  })
  .catch((err) => next({status:500, err}))
}

/**
 * Gets all items
 * Optionally receives a `completed` param
 * that can be true or false, to filter items
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const read = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const completed = ( 'completed' in req.params && typeof req.params.completed !== 'undefined' && req.params.completed !== ''
  ? completedParamToArgument(req.params.completed)
  : null
  )
  model.read({completed})
    .then( (todos:any ) => {
      sendBack(req, res, todos)
    })
    .catch((err:Error) => next({status:500, err}))
}

/**
 * Updates one item
 * Needs:
 * - an `id` parameter
 * - a `completed` GET param or POST param
 * - a `text` GET param or POST param
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const update = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const text = req.body.text || req.query.text
  const completed = req.body.completed || req.query.completed
  const { id } = req.params
  if(!text && !completed){ return next({ status:403, err:new Error('modifying a todo requires either the `text` and `completed` properties')})}
  model.update(id, text, completed).then( (modified_rows) => {
    if(modified_rows){
      return sendBack(req, res)
    }
    next({status:500, err:new Error('request did not succeed')})
  })
  .catch((err) => next({status:404, err}))
}

/**
 * Toggles an item from completed to uncompleted
 * and vice versa. Needs an `id` parameter
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const toggle = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const { id } = req.params
  model.toggle(id).then( (modified_rows) => {
    if(modified_rows){
      return sendBack(req, res)
    }
    next({status:500, err:new Error('request did not succeed')})
  })
  .catch((err) => next({status:404, err}))
}

/**
 * Toggles all items
 * Needs an `completed` parameter
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const toggleAll = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  if(!('completed' in req.params) || typeof req.params.completed === 'undefined' || req.params.completed === null){
    return next({status:500, err:new Error('You need a `completed` parameter')})
  }
  const completed = completedParamToArgument(req.params.completed)
  model.toggleAll(completed).then( (modified_rows) => {
    if(modified_rows){
      return sendBack(req, res)
    }
    next({status:500, err:new Error('request did not succeed')})
  })
  .catch((err) => next({status:404, err}))
}

/**
 * Function that deletes one item
 * Needs an `id` parameter
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const remove = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const { id } = req.params
  model.remove(id).then( (modified_rows) => {
    if(modified_rows){
      return sendBack(req, res)
    }
    next({status:500, err:new Error('request did not succeed')})
  })
  .catch((err) => next({status:404, err}))
}

/**
 * Function that deletes all completed items
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const removeCompleted = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  model.removeCompleted().then( (modified_rows) => {
    if(modified_rows){
      return sendBack(req, res)
    }
    next({status:500, err:new Error('request did not succeed')})
  })
  .catch((err) => next({status:404, err}))
}

/**
 * Adds a todo item
 * Needs a `text` parameter on GET or POST
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const add = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const text = req.body.text || req.query.text
  const completed = completedParamToArgument(req.body.completed || req.query.completed)
  if(!text){ return next({ status:403, err:new Error('adding a todo requires providing a `text` property ')}) }
  model.add(text)
    .then( todo => {
      return sendBack(req, res, todo)
    })
    .catch((err) => next({status:500, err}))
}

/**
 * Function that creates the table
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const createTable = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  model.createTable()
  .then( () => sendBack(req, res))
  .catch( err => next({status:500, err}))
}

/**
 * Function that inserts initial rows into the table
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const insertRows = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  const rows = req.body.todos
  model.insertRows(rows)
    .then( todos => sendBack(req, res, todos))
    .catch( err => next({status:500, err}))
}

/**
 * Function that sets initial parameters for the
 * table. Creates the table if it doesn't exist,
 * and inserts a few rows
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const seed = (req:express.Request, res:express.Response, next:express.NextFunction) => {
  model.seed()
  .then( () => sendBack(req, res))
  .catch( err => next({status:500, err}))
}

/**
 * Error handler for the SQL api
 * @param error the error
 * @param req the request
 * @param res the response
 * @param next next possible function
 */
const errorHandler = (error:{status:number, err:Error}|Error, req:express.Request, res:express.Response, next:express.NextFunction) => {
  if(error){
    const [ message, status, _stack ] = ( error
    ? (  error instanceof Error
      ? [error.message, 500, error.stack || '']
      : ( error.err ? [error.err.message, error.status, error.err.stack || ''] : ['unknown', 500, ''] )
      )
    : ['unknown', 500, '']
    )
    const num = status || 500
    const stack = _stack.split('\n')
    const url = req.originalUrl
    if (isAjaxQuery(req)) {
      res.status(num).send({ status:'error', code:num, error: message, stack, url})
    } else {
      res.status(num).send(`<html><body><h1>Error ${num}</h1><p>${url}</p><p>${message}</p><pre>${stack.map(s => `${s}<br>\n`)}</pre></body></html>`)
    }
  }
}

export const router = express()

router.use(bodyParser.json())
router.use(bodyParser.urlencoded({extended: true}))

router.all('/seed', seed)
router.all('/get/:id', getOne)
router.all('/all/:completed?', read)
router.all('/update/:id', update)
router.all('/toggle/:id', toggle)
router.all('/delete/:id', remove)
router.all('/toggle-all/:completed', toggleAll)
router.all('/remove-completed', removeCompleted)
router.all('/new', add)
router.get('/:id', getOne)
router.get('/', read)

router.put('/:id', update)
router.delete('/:id', remove)
router.post('/', add)

router.use(errorHandler)