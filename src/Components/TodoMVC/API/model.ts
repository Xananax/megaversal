/**
 * An example of a simple SQL app
 * Although usable, I *highly* recommend not doing things
 * this way. Friends don't let friends write SQL queries by hand.
 * I invite you to check Knex, which will make your life much easier
 */
import * as sqlite from 'sqlite3'

interface Todo{
  id:string|number
  text:string
  completed:boolean
}

const examples:Todo[] = [
  { id:'a', text: 'Try SQLite', completed: true },
  { id:'b', text: 'Try Knex', completed: false }
]

interface Props{
  filename?:string
  rows?:boolean
}

export interface ReadOptions{
  orderBy:string
  direction:'ASC'|'DESC'
  limit:number
  offset:number
  completed:boolean|null
}

const tableName = 'todos'

// use :memory: instead of a filename for temporary storage
const db = new sqlite.Database('todos.sqlite')

/**
 * creates the table if it doesn't exist
 */
export const createTable = ():Promise<sqlite.Database> => new Promise( (resolve, reject) => {
  db.run(`CREATE TABLE IF NOT EXISTS ${tableName} (text TEXT, completed BOOL)`, (err) => {
    if(err){
      return reject(err)
    }
    resolve(db)
  })
})

/**
 * inserts a bunch of rows in the table
 * @param rows an array of rows
 */
export const insertRows = (rows:Todo[]):Promise<Todo[]> => new Promise( (resolve, reject ) => {
  var stmt = db.prepare(`INSERT INTO ${tableName} (text,completed) VALUES ($text,$completed)`)
  rows.map(({text:$text, completed:$completed}) => stmt.run({$text, $completed}))
  stmt.finalize(function(statement_error:Error){
    if(statement_error){
      return reject(statement_error)
    }
    return resolve(rows)
  })
})

/**
 * Creates the table, then feeds it a bunch of example rows
 */
export const seed = () => createTable().then(() => insertRows(examples)).catch(err => {throw err})

/**
 * reads all items from the database
 * @param opts list options. Default to order by id ascending, limited to 100 items
 */
export const read = (opts?:Partial<ReadOptions>):any => new Promise( (resolve, reject) => {

  const {
    orderBy: $orderBy,
    direction,
    limit: $limit,
    offset: $offset,
    completed
  } = { orderBy:'id', direction:'ASC', limit:100, offset:0, completed:null, ...opts} as ReadOptions

  const $dir = ( direction
  ? ( direction.toUpperCase() === 'DESC '
    ? 'DESC'
    : 'ASC'
    )
  : 'ASC'
  )

  const $completed = ( completed === false
  ? '0'
  : ( completed === true
    ? '1'
    : '0 OR 1'
    )
  )

  const query = [
    `SELECT rowid AS id, text, completed FROM ${tableName}`,
    `WHERE completed=${$completed}`,
    `ORDER BY $orderBy ${$dir}`,
    `LIMIT $limit OFFSET $offset`
  ]
  .filter(Boolean)
  .join(' ')

  const replacements:{[key:string]:any} = { $orderBy, $limit, $offset }

  db.all(query, replacements, (err,rows) => {
    if(err){
      // err.message = query.replace(/\$\w+/g,(n)=>replacements[n]);
      return reject(err)
    }
    return resolve(rows)
  })

})

/**
 * Returns one item by id
 * @param $id the id of the item to return
 */
export const getOne = ($id:string|number):Promise<Todo[]> => new Promise( (resolve, reject) => {
  return db.get(`SELECT rowid AS id, text, completed FROM ${tableName} WHERE id = $id`, {$id}, (err?:Error, row?:Todo) => {
    if(err){ return reject(err)}
    return resolve([row as Todo])
  })
})

/**
 * closes the database connection
 */
export const close = () => Promise.resolve(db.close())

/**
 * Adds one item to the database
 * @param text text of the item
 */
export const add = (text:string):Promise<Todo> => new Promise( (resolve, reject) => {
  const $text = text.trim()
  const $completed = false
  return db.run(`INSERT INTO ${tableName} (text,completed) VALUES ($text,$completed)`, {$text, $completed}, function(err?:Error){
    if(err){ return reject(err)}
    const id = this.lastID
    const todo:Todo = {id, text:$text, completed:$completed}
    resolve(todo)
  })
})

/**
 * Removes one item from the database
 * @param $id the id of the item
 */
export const remove = ($id:string):Promise<number> => new Promise( (resolve, reject) => {
  if(!$id){ return Promise.reject(`id was not specified`)}
  return db.run(`DELETE FROM ${tableName} WHERE rowid = $id`, {$id}, function(err?:Error){
    if(err){ return reject(err)}
    const numRows = this.changes
    return resolve(numRows)
  })
})

/**
 * Removes completed items from the database
 */
export const removeCompleted = ():Promise<number> => new Promise( (resolve, reject) => {
  return db.run(`DELETE FROM ${tableName} WHERE completed=$completed`, {$completed:true}, function(err?:Error){
    if(err){ return reject(err)}
    const numRows = this.changes
    return resolve(numRows)
  })
})

/**
 * modifies one item in the database
 * @param $id the id of the item to modify
 * @param $text the text of the item to modify
 * @param $completed if the item is completed or not
 */
export const edit = ($id:string, $text:string, $completed: boolean):Promise<number> => new Promise( (resolve, reject) => {
  db.run(`UPDATE ${tableName} SET text = $text, completed = $completed WHERE rowid = $id`, {$id, $text, $completed}, function(err?:Error){
    if(err){ return reject(err)}
    const numRows = this.changes
    return resolve(numRows)
  })
})

/**
 * updates one item in the database.
 * Any properties that aren't passed will not be modified
 * @param $id the id of the item to modify
 * @param text the text of the item to modify
 * @param completed if the item is completed or not
 */
export const update = ($id:string, text:string, completed: boolean|string):Promise<number> => {
  return getOne($id)
    .then( ([ item ]) => {
      if(!item){
        throw new Error(`item \`${$id}\` not found`)
      }
      const $text = ( typeof text === 'undefined' || text === null || text === ''
      ? item.text
      : text
      )
      const $completed = !!( typeof completed === 'undefined' || completed === null || completed === ''
      ? item.completed
      : completed
      )
      return edit($id,$text,$completed)
    })
}

/**
 * Toggles one item from completed to not completed and
 * vice versa
 * @param $id the id of the item to modify
 */
export const toggle = ($id:string):Promise<number> => {
  return getOne($id)
    .then( ([ item ]) => {
      if(!item){
        throw new Error(`item \`${$id}\` not found`)
      }
      const $text = item.text
      const $completed = !item.completed
      return edit($id,$text,$completed)
    })
}

/**
 * Completes or uncompletes all todos
 * @param $completed
 */
export const toggleAll = ($completed: boolean|number):Promise<number> => new Promise( (resolve, reject) => {
  db.run(`UPDATE ${tableName} SET completed=$completed`, {$completed}, function(err?:Error){
    if(err){ return reject(err)}
    const numRows = this.changes
    return resolve(numRows)
  })
})
