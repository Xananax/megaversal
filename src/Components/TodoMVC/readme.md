# TODO MVC

This example reproduces the [todo mvc](todomvc.com) patterns and as such might not be the best example of how to build a todo app; but it benefits from being easy to compare with other TodoMVC implementations

It needs a store to save and load data, of which two are available in `Stores/`