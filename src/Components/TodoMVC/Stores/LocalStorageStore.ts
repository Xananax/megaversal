import { TodoStore, Todo, TODOS_TYPES } from '../types'
import { uuid } from '../utils'

const isClient = process.env.BUILD_TARGET === 'client'

export const save = ( isClient
  ? (namespace: string, data: any[]) => {
      localStorage.setItem(namespace, JSON.stringify(data))
      return data
    }
  : (namespace: string, data: any[]) => []
)

export const load = ( isClient
  ? (namespace:string) => {
      var _store = localStorage.getItem(namespace)
      return (_store && JSON.parse(_store)) || []
    }
  : (namespace:string) => []
)

export class LocalStorageStore implements TodoStore{

  key: string
  completedCount: number
  activeCount: number
  todos: Todo[]
  _onChanges: ((todos: Todo[]) => any)[]

  constructor(key:string) {
    this.key = key
    this.todos = load(key)
    this._onChanges = []
  }

  get(which?:TODOS_TYPES){
    if(which && which === 'active'){
      return Promise.resolve(this.todos.filter(({completed}) => !completed))
    }
    if(which && which === 'completed'){
      return Promise.resolve(this.todos.filter(({completed}) => completed))
    }
    return Promise.resolve(this.todos)
  }

  onChange(onChange: (todos: Todo[]) => any) {
    this._onChanges.push(onChange)
  }

  trigger() {
    save(this.key, this.todos)
    this.activeCount = this.todos.reduce(
      (count:number , todo:Todo) => (todo.completed ? count : count + 1),
      0
    )
    this.completedCount = this.todos.length - this.activeCount
    this._onChanges.forEach( cb => cb(this.todos) )
  }

  add(text: string) {
    const newTodo = {
      id: uuid(),
      text: text,
      completed: false
    }
    this.todos = [...this.todos, newTodo]
    this.trigger()
    return Promise.resolve(newTodo)
  }

  toggleAll(completed: boolean) {
    this.todos = this.todos.map(
      (todo: Todo) =>
      ({ ...todo, completed })
    )
    this.trigger()
    return Promise.resolve()
  }

  toggle(id:string) {
    this.todos = this.todos.map(
      ( todo ) =>
      ( todo.id !== id
      ? todo
      : {...todo, completed:!todo.completed}
      )
    )
    this.trigger()
    return Promise.resolve()
  }

  destroy(id:string) {
    this.todos = this.todos.filter((todo) => todo.id !== id )
    this.trigger()
    return Promise.resolve()
  }

  update(id: string, text: string) {
    this.todos = this.todos.map(
      (todo) =>
      ( todo.id !== id ? todo : { ...todo, text})
    )
    this.trigger()
    return Promise.resolve()
  }

  clearCompleted() {
    this.todos = this.todos.filter( (todo) => !todo.completed )
    this.trigger()
    return Promise.resolve()
  }
}