# Stores

This contains two stores to plug into the TodoMVC app

## LocalStorageStore

Is an in-memory store. Saves everything in arrays; however, it serializes all items and writes them to the browser's localStorage after each operation. It also hydrates its data from localStorage upon loading


## ApiStore

Is a store using `fetch` to send requests to a server API. Relies on `fetch` being available, and the API being mounted on `/api/sql`