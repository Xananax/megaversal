import { TodoStore, Todo, TODOS_TYPES } from '../types'
import { uuid } from '../utils'

const isClient = process.env.BUILD_TARGET === 'client'
const basePath = '/api/todos'

const get = (params:string) => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json')
  headers.append('X-Requested-With', 'XMLHttpRequest')
  return fetch(basePath+params,{
    method:'get',
    headers
  })
    .then( results => results.json() )
    .then( results => {
      if( results.status === 'success'){
        return results.data
      }
      const {error:message}= results;
      throw new Error(message)
    })
}
export class ApiStore implements TodoStore{

  completedCount: number
  activeCount: number
  _onChanges: ((todos: Todo[]) => any)[]

  constructor(key?:string) {
    this._onChanges = []
    this.trigger = this.trigger.bind(this)
    this.get = this.get.bind(this)
  }

  onChange(onChange: (todos: Todo[]) => any) {
    this._onChanges.push(onChange)
  }

  trigger(todos:Todo[]) {
    this.activeCount = todos.reduce(
      (count:number , todo:Todo) => (todo.completed ? count : count + 1),
      0
    )
    this.completedCount = todos.length - this.activeCount
    this._onChanges.forEach( cb => cb(todos) )
    return todos
  }

  get(which?:TODOS_TYPES){
    if(which && which === 'active'){
      return get('/all/0').then(this.trigger)
    }
    if(which && which === 'completed'){
      return get('/all/1').then(this.trigger)
    }
    return get('/all').then(this.trigger)
  }

  add(text: string) {
    return get(`/new/?text=${text}`)
      .then(addedTodo => {
        return this.get()
          .then(this.trigger)
          .then(()=>addedTodo)
          .catch(err => {throw err})
      })
  }

  toggleAll(completed: boolean) {
    return get(`/toggle-all/${completed?1:0}`).then(this.get)
  }

  toggle(id:string) {
    return get(`/toggle/${id}`).then(this.get)
  }

  destroy(id:string) {
    return get(`/delete/${id}`).then(this.get)
  }

  update(id: string, text: string) {
    return get(`/update/${id}?text=${text}`).then(this.get)
  }

  clearCompleted() {
    return get('/remove-completed').then(this.get)
  }
}