import App from './App'
import * as React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

describe('<App />', () => {
  test('renders without exploding', () => {
    const div = document.createElement('div')
    render(
      <BrowserRouter>
        <App />
      </BrowserRouter>,
      div
    )

  })
})
