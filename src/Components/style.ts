import { style, prepare, media } from '../helpers/style'
import { rem, em, px, white, percent } from 'csx'

prepare('#Root', { fontFamily: 'sans-serif' })

export const main = style({
  $debugName: `home`,
  textAlign: `center`,
  minHeight:percent(100),
  width:percent(100)
})

export const container = style(
  {
    width:px(320),
    margin:'0 auto'
  },
  media( {minWidth:640, maxWidth: 719}, {width:px(640)}),
  media( {minWidth:720, maxWidth: 959}, {width:px(720)}),
  media( {minWidth:960},                {width:px(960)}),
)

export const header = style({
  $debugName: `header`,
  backgroundColor: `#222`,
  height: px(150),
  padding: px(20),
  color: white.toString()
})

export const intro = style({
  $debugName: `intro`,
  fontSize: `large`
})

export const resources = style({
  $debugName: `resources`,
  listStyle: `none`,
  $nest: {
    '& li': {
      display: `inline-block`,
      padding: rem(1)
    }
  }
})

export const nav = style({
  $debugName:`nav`,
  $nest: {
    '& a':{
      display:`inline-block`,
      textDecoration:`none`,
      color:`#fff`,
      background:`#000`,
      padding:px(10),
      marginLeft:px(2),
      marginRight:px(2)
    },
    '& a.active':{
      color:`red`
    }
  }
})

export const footer = style({
  $debugName:`footer`,
  paddingTop:px(2),
  paddingBottom:px(2),
  background:`black`,
  color:`white`,
  fontSize:em(.7),
  width:percent(100),
  textAlign:`center`,
  zIndex:10
})

const classes = {
  main,
  header,
  intro,
  resources
}

export default classes
