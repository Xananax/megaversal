import * as React from 'react'
import Helmet from 'react-helmet'
import { App as TodoApp, LocalStorageStore } from '../TodoMVC'

const store = new LocalStorageStore('todo-mvc')

export const TodoLocal = () =>
  <div>
    <Helmet>
      <title>Todo App + LocalStorage</title>
    </Helmet>
    <TodoApp store={store}/>
  </div>