import * as React from 'react'
import Helmet from 'react-helmet'
import { App as TodoApp, ApiStore } from '../TodoMVC'

const store = new ApiStore()

export const TodoAPI = () =>
  <div>
    <Helmet>
      <title>Todo App + API</title>
    </Helmet>
    <TodoApp store={store}/>
  </div>