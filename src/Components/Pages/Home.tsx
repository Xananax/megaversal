import * as React from 'react'
import { Component } from 'react'
import * as classes from '../style'
import Helmet from 'react-helmet'
import { Raw } from  '../utils/Raw'
import * as readme from '../../readme.md'

export const Home = () => (
  <div>
    <Helmet>
      <title>Home</title>
    </Helmet>
    <p className={classes.intro}>
      <span>To get started, edit </span>
      <code>{__filename}</code>
      <span> and save to reload.</span>
    </p>
    <Raw>{readme}</Raw>
  </div>
)