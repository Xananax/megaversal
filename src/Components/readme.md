# Components

Holds all the component used by the React part of the application.

Stateful components are kept to a minimum, with as many "dumb" components as possible.

All components render similarly on the server and in the browser