/**
 * List of all events used or that may
 * be used in the app
 */

// EVENTS RESERVED BY SOCKET.IO
export const CONNECT = 'connection'
export const DISCONNECT = 'disconnect'
export const ERROR = 'error'
export const DISCONNECTING = 'disconnecting'
export const NEW_LISTENER = 'newListener'
export const REMOVE_LISTENER = 'removeListener'
export const PING = 'ping'
export const PONG = 'pong'

// CUSTOM EVENTS
export const MESSAGE = 'message'
export const NEW_USER_ENTER = 'new_user_enter'
export const NEW_USER_EXIT = 'new_user_exit'
export const NEW_NICK = 'new_nick'