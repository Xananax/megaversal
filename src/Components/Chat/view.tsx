/**
 * React app that interacts through socket.io
 * with the server
 */
import * as React from 'react'
import { Component } from 'react'
import * as io from 'socket.io-client'
import * as Events from './events'

interface LineProps{
  user:string
  message:string
}

interface State{
  user:string
  messages:LineProps[]
  users:string[]
}

const Line = ({user,message}:LineProps) =>
  <div style={{width:'100%',borderBottom:'1px solid black'}}>
    <span style={{width:'200px',display:'inline-block'}}>{user}</span>
    <span>{message}</span>
  </div>

export class Chat extends Component<{},State>{

  socket:SocketIOClient.Socket

  constructor(){
    super()
    this.state = {
      user:'',
      messages:[],
      users:[]
    }
  }

  addMessage = (user:string,message:string) => {
    const messages = [...this.state.messages, {user,message}]
    this.setState({messages})
  }

  sendMessage = (evt:React.KeyboardEvent<HTMLInputElement>) => {
    const key = evt.charCode || evt.keyCode;
    if(key !== 13){return;}
    const el = evt.target as HTMLInputElement
    const message = el.value.trim()
    if(!message){return;}
    this.socket.emit(Events.MESSAGE,message)
    el.value = ''
  }

  componentDidMount(){
    const socket = io()
    this.socket = socket
    socket.on(Events.NEW_USER_ENTER,({user}:{user:string})=>{
      this.addMessage('SYSTEM: new user',user)
    })
    socket.on(Events.NEW_USER_EXIT,({user}:{user:string})=>{
      this.addMessage('SYSTEM: new user',user)
    })
    socket.on(Events.MESSAGE,({user,message}:{user:string,message:string})=>{
      this.addMessage(user,message)
    })
    socket.on(Events.NEW_NICK,({user,users}:{user:string,users:string[]})=>{
      this.setState({user,users})
    })
  }

  render(){
    const { user:nick, users, messages } = this.state
    return (
    <div>
      <div style={{width:'100%'}}>
          <h2>{nick}</h2>
      </div>
      <div style={{width:'100%',height:'90%',textAlign:'left'}}>
        <div style={{width:'80%',float:'left'}}>
          {messages.map(({user,message},key)=><Line key={key} {...{user,message}}/>)}
        </div>
        <div style={{width:'20%',float:'left'}}>
          {users.map(user=><p key={user}>{user}</p>)}
        </div>
      </div>
      <div style={{width:'100%'}}>
        <input defaultValue="" onKeyUp={this.sendMessage}/>
      </div>
    </div>)
  }
}
