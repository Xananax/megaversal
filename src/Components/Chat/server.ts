/**
 * The server side part of a simple chat app
 * All it does it assign a random nick to a user
 * and send it back, as well as dispatch messages
 * to all connected users
 */

import * as makeSocket from 'socket.io'
import * as Events from './events'
import { getRandomNick } from './utils/getRandomNick'
import { Server } from 'http'

const users:string[] = []

const bindToServer = (server?:Server) => {

  const io = makeSocket(server)

  const socketList:SocketIO.Socket[] = []

  // this function runs every time someone connects
  // `socket` will be the unique connection with a
  // specific browser/user
  io.on(Events.CONNECT, ( socket ) => {
    // add socket to the list
    socketList.push(socket)
    // generate a user nick
    const user = getRandomNick()
    // add the user to the users array
    users.push(user)
    // broadcast to the user the nick and the list of users
    socket.emit(Events.NEW_NICK,{user,users})
    // broadcast to all users the new user
    io.emit(Events.NEW_USER_ENTER,{user})

    socket.on(Events.DISCONNECT, () => {
      const socketIndex = socketList.indexOf(socket)
      socketList.splice(socketIndex, 1)
      const index = users.indexOf(user)
      if(index >= 0){
        users.splice(index,1)
        io.emit(Events.NEW_USER_EXIT,{user})
      }
    })

    // when a message is received from a user
    socket.on(Events.MESSAGE, (message:string) => {
      // ... dispatch it to all users
      io.emit(Events.MESSAGE,{user,message})
    })

  })

  const shutdown = () => {
    socketList.forEach((socket) => {
      socket.disconnect()
    })
    io.close()
  }

  return shutdown

}

export default bindToServer