import { BrowserRouter } from 'react-router-dom'
import * as React from 'react'
import { render } from 'react-dom'
import App from './Components/App'
import { setStylesTarget } from './helpers/style'

const appTarget = document.getElementById('Root')
const stylesTarget = document.getElementById('Styles') as HTMLStyleElement

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  appTarget
)

setStylesTarget(stylesTarget)

if (module.hot) {
  module.hot.accept()
}