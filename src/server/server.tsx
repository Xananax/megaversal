import * as express from 'express'
import api from './api'
import blog from './blog'
import { createReactHandler } from './react-app'
import { getStyles } from '../helpers/style'
import App from '../Components/App'

const assets = require(process.env.RAZZLE_ASSETS_MANIFEST as string)

const app = express()

app
  .disable('x-powered-by')
  .use(express.static(process.env.RAZZLE_PUBLIC_DIR || 'public'))
  .use('/api', api)
  .use('/blog', blog)
  .get('/*', createReactHandler({App,getStyles,assets}))

export default app
