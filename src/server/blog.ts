/**
 * # Blog
 *
 * A simple, crude, brute-forced CRUD app written in Express.
 *
 * Blog posts are stored in an array in memory (and will disappear if the server restarts)
 *
 * Rendering is done through writing strings.
 *
 * In an actual application, posts would be stored in a database (or in the filesystem), and rendering would be done through a rendering engine, hopefully with streaming capabilities.
 *
 * Once again, this is not here to demonstrate best practices, but to be kind of easily understood and taken apart.
 */
import * as express from 'express'
import * as bodyParser from 'body-parser'
import { sortArrayByItemProperty } from  '../helpers/sortArrayByItemProperty'

interface Post{
  title:string
  text:string
}

////////////////////////////////////////////////////
// DATA
// Just creating some data
////////////////////////////////////////////////////

/**
 * A few seeds posts so we don't start entirely empty
 */
const posts:Post[] = [
  {title:'ZZZZ', text:'Nulla vitae dolor non enim laoreet semper non a lectus. Integer rhoncus dolor et ante venenatis, vel volutpat turpis cursus. Praesent sodales eleifend euismod. Praesent mauris massa, blandit eget metus eget, laoreet rutrum tellus. Ut nisl sem, viverra eget leo vel, sagittis pretium nisl. Donec porta enim non odio convallis, non blandit ipsum auctor. Phasellus commodo erat leo, sed cursus mauris semper eu. Donec rutrum erat magna, vitae volutpat nulla pellentesque in. Cras augue mauris, tincidunt id suscipit viverra, luctus nec lacus'},
  {title:'B blog post', text:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam fermentum accumsan rhoncus. Quisque sodales dui dolor, at imperdiet dui sollicitudin ut. Integer a ultricies tortor. Ut venenatis erat at nulla mollis, blandit commodo augue pellentesque. Nullam dictum venenatis vulputate. Maecenas vitae ultricies mi. Suspendisse volutpat, felis sed dignissim convallis, velit ex facilisis leo, vitae bibendum urna mauris at nisl. Nullam eget arcu sit amet libero vehicula finibus. Vivamus at erat urna. Nullam placerat erat eu diam vestibulum lobortis.'},
  {title:'A blog post', text:'Nulla vitae dolor non enim laoreet semper non a lectus. Integer rhoncus dolor et ante venenatis, vel volutpat turpis cursus. Praesent sodales eleifend euismod. Praesent mauris massa, blandit eget metus eget, laoreet rutrum tellus. Ut nisl sem, viverra eget leo vel, sagittis pretium nisl. Donec porta enim non odio convallis, non blandit ipsum auctor. Phasellus commodo erat leo, sed cursus mauris semper eu. Donec rutrum erat magna, vitae volutpat nulla pellentesque in. Cras augue mauris, tincidunt id suscipit viverra, luctus nec lacus'},
]

////////////////////////////////////////////////////
// RENDER FUNCTIONS
// functions that return strings
// used in pages or to render pages
////////////////////////////////////////////////////

/**
 * Renders a page
 * @param title title of the page
 * @param body body of the page
 */
const render = (title:string, body:string) => `<html>
<head><title>${title}</title></head>
<body><h1>${title}</h1><div>${body}</div></body>
</html>`

/**
 * Renders a form
 * @param title the title value of the title input field
 * @param text the text value of the text input field
 * @param additional any additional text you want to add. It'll be appended to the form below the text input
 */
const create_form = (title?:string, text?:string, additional?:string) => `<form method="post">
  <div><label>Title<input name="title" value="${title ? title : ''}"></label></div>
  <div><label>Text<textarea cols=20 rows=15 name="text">${text ? text : ''}</textarea></label></div>
  ${additional ? additional : '' }
  <input type="submit" value="post"/>
</form>`

/**
 * renders a hidden input with name `input`
 * @param id the value of the input
 */
const id_input = (id:number) => `<input type="hidden" value="${id}" name="id"/>`

/**
 * renders a form with a hidden "id" field
 * @param id the id of the post
 * @param title title of the post
 * @param text text of the post
 */
const edit_form = (id:number, title?:string, text?:string) => create_form(title, text, id_input(id))

/**
 * Renders a post in full format
 * @param Post an object containing a title and a text properties
 * @param id the post's id
 */
const render_single_post = ({title, text}:Post, id:number) =>
  `<div id="post_${id}">
  <h3>${title}</h3>
  <p>${text}</p>
  <a href="/blog/edit/${id}">edit</a>
  </div>`

/**
 * Renders a post in summary format
 * @param Post an object containing a title and a text properties
 * @param id the post's id
 */
const render_single_post_summary = ({title, text}:Post, id:number) =>
  `<div id="post_${id}">
  <h3>${title}</h3>
  <p>
    ${text.slice(0, 10) + '...' }
    <a href="/blog/post/${id}">read full article</a>
  </p>
  <a href="/blog/edit/${id}">edit</a>
  </div>`

////////////////////////////////////////////////////
// SERVER
// Here we set up the express app,
// which will answer http requests
////////////////////////////////////////////////////

/**
 * creates an Express router
 */
const app = express()

/**
 * Enables the express router to use
 * POST requests
 * Each of these functions parses the body
 * (either when sent as json or as an URL-encoded string)
 * and sets the results on the `body` property of the
 * `request` object
 */

const jsonParser = bodyParser.json()
app.use(jsonParser)

const urlencodedParser = bodyParser.urlencoded({ extended: false })
app.use(urlencodedParser)

////////////////////////////////////////////////////
// ROUTING
// Setting up the routes, and what they should
// send back, depending on the requests the express
// app receives
////////////////////////////////////////////////////

/**
 * this route answers to requests such as
 * `/blog/post/0` or `/blog/post/3`
 */
app.all('/post/:id', (request, response, next) => {
  const {id} = request.params
  if(posts[id]){
    const rendered_post = render(posts[id].title, render_single_post(posts[id], id))
    response.send(rendered_post)
  }else{
    next(new Error(`post ${id} not found`))
  }
})

/**
 * This route loads a form on GET requests
 * and renders a page on POST requests
 * It's the same url: `/create`
 * But we discriminate on the http *method*
 */
app.get('/create', (req, res, next) => {
  const form = create_form()
  const page = render('enter a new blog post', form)
  res.send(page)
})

app.post('/create', (req, res, next) => {
  const {text, title} = req.body
  const new_post = {text, title}
  const id = posts.length
  posts.push(new_post)
  const post_rendered = render_single_post(new_post, id)
  const page = render(new_post.title, post_rendered)
  res.send(page)
})

/**
 * Another way to differenciate between POST
 * and GET; instead of using `app.get` and `app.post`,
 * I use `app.all` and differenciate within the same
 * request handler using `request.method`
 */
app.all('/edit/:id', (req, res, next) => {
  const {id} = req.params
  const post = posts[id]
  if(!post){
    return next(new Error(`post ${id} not found`))
  }
  if(req.method === 'GET'){
    const form = edit_form(id, post.title, post.text)
    const page = render(`edit post ${post.title}`, form)
    return res.send(page)
  }else if(req.method === 'POST'){
    const { title, text } = req.body
    const new_post = {title, text}
    posts[id] = new_post
    const rendered_post = render_single_post(new_post, id)
    const page = render(new_post.title, rendered_post)
    return res.send(page)
  }
  next()
})

/**
 * Handles the main page
 * with optional parameter
 */
app.all('/:order?', (request, response, next) => {
  const order = request.params.order || 'index'
  const posts_list = ( order === 'index' || !order
  ? posts
  : posts.sort(sortArrayByItemProperty(order))
  )
  const posts_rendered = posts_list.map(render_single_post_summary).join('\n')
  const page = render(`Blog posts by ${order}`, posts_rendered)
  response.send(page)
})

/**
 * Global handler for when  nothing
 * else has worked
 */
app.use( (req, res, next) => {
  res.status(404).send('not found')
})

export default app