/**
 * this file keeps all the REST Apis used by the components
 * under its umbrella.
 *
 * Normally, you'd put all the files pertaining
 * to the server in the server's directory, but in this case,
 * I preferred keeping API and component close to each other
 */

import * as express from 'express'
import todos from '../Components/TodoMVC/API'
import authors from '../Components/Authors/API'

const endpoints:{[key:string]:express.Express} = {
  todos,
  authors
}

const endpointsNames = Object.keys(endpoints)

const app = express()

endpointsNames.forEach( k => {
  const path = '/'+k;
  const subApp = endpoints[k]
  app.use(path, subApp);
})

app.use('/', (req, res, next) => {
  res.send({endpoints:endpointsNames})
})

export default app