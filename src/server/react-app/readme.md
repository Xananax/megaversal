# React-App

This server-side express app takes care of rendering a React App.   
All routes will be passed to `react-router`, which will handle them from there on

The renderer uses `react-helmet`, so you can just pass options there to have them reflected in the final rendered template