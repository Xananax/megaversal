import * as React from 'react'
import { StaticRouter } from 'react-router-dom'
import { renderToString } from 'react-dom/server'

/**
 * Renders a react app to string, and wraps it in `react-router`'s `StaticRouter`
 * to make routing work server-side
 * @param App The react app
 * @param context the router context
 * @param url the user submitted url
 */
export const renderReact = ( App:any, context: object, url: string ) => {
  return renderToString(
    <StaticRouter context={context} location={url}>
      <App />
    </StaticRouter>
  )
}
