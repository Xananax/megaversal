import Helmet from 'react-helmet'

export interface Assets {
  client: {
    js: string
    css: string
  }
}

export interface RenderOptions {
  assets: Assets
  markup: string
  styles: string
}

const scriptTag = (src:string) => {
  if(!src){ return '' }
  return ( process.env.NODE_ENV === 'production'
  ? `<script src="${src}" defer></script>`
  : `<script src="${src}" defer crossorigin></script>`
  )
}

const cssTag = (src:string) => {
  return ( src
    ? `<link rel="stylesheet" href="${src}">`
    : ''
  )
}

const styleTag = (styles:string) => {
  return ( styles
  ? `<style id="Styles">${styles}</style>`
  : ''
  )
}

/**
 * Takes an options object and returns a valid HTML page as a string
 * @param opts an object of options
 */
export const renderPage = ( options: RenderOptions) => {
  const {
    assets: {
      client: { js, css }
    },
    markup,
    styles
  } = options
  const {
    base,
    bodyAttributes,
    htmlAttributes,
    link,
    meta,
    noscript,
    script,
    style,
    title
  } = Helmet.renderStatic()
  return (
  `<!doctype html>` +
  `<html ${htmlAttributes}>` +
    `<head>` +
        meta +
        title +
        cssTag(css) +
        link +
        styleTag(styles) +
        style +
        script +
        scriptTag(js) +
        base +
    `</head>` +
    `<body ${bodyAttributes}>` +
      noscript +
      `<div id="Root">` +
        markup +
      `</div>` +
    `</body>` +
  `</html>`
  )
}