import { Request, Response } from 'express'
import { renderReact } from  './renderReact'
import { renderPage } from './renderPage'

export interface CreateReactHandlerOptions{
  App:any
  getStyles:()=>string
  assets:any
}

/**
 * Takes an object of options and returns an express handler that renders a react app to the page
 * @param options
 */
export const createReactHandler = ({App,getStyles,assets}:CreateReactHandlerOptions) => {

  const handler = (req:Request, res:Response) => {

    const context: any = {}
    const markup = renderReact(App,context, req.url)

    if (context.url) {
      res.redirect(context.url)
    } else {
      const options = {
        assets,
        markup,
        styles: getStyles()
      }
      res.status(200).send(renderPage(options))
    }
  }

  return handler
}