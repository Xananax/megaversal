/**
 * Creates a sorter function suitable for sorting arrays of objects.
 * For example, if you have an array like so:
 *
 * ```
 * const people = [{name:'B'},{name:'C'},{name:'A'}]
 * ```
 * You can sort it this way:
 *
 * ```
 * const sorter = sortArrayByItemProperty('name')
 * people.sort(sorter)
 * ```
 * @param propertyName a string that represents a property present on all members of an array
 */
export const sortArrayByItemProperty =
  <T, K extends keyof T>
  ( propertyName:K ) =>
  ( a:T, b:T ) => {
    return ( a[propertyName] < b[propertyName]
    ? - 1
    : ( a[propertyName] > b[propertyName]
      ? 1
      : 0
      )
    )
  }