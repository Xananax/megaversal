/**
 * Function that waits a bit.
 * Mainly used to simulate longer server roundtrips, e.g, for testing.
 * Use it like so:
 * ```
 * fetch(url).then(w ait(200)).then(response=>response.json()).then(...)
 * ```
 * If you pass 0, the promise will just run immediatly
 * @param milliseconds the amount of wait
 */
export const wait =
  (milliseconds:number=500) =>
  <T>(thing:T):Promise<T> =>
  new Promise( resolve => ( milliseconds
    ? setTimeout(()=>resolve(thing),milliseconds)
    : resolve(thing)
  ))