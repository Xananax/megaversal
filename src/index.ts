import app from './server/server'
// import bindChat from './Components/Chat/server'
import * as http from 'http'

const server = http.createServer(app)

let currentApp = app

// bindChat(server)

server.listen(process.env.PORT || 3000)

if (module.hot) {
  // tslint:disable-next-line:no-console
  console.log('✅  Server-side HMR Enabled!')

  module.hot.accept(['./server/server'/*,'./Components/Chat/server'*/], () => {
    // tslint:disable-next-line:no-console
    console.log('🔁  HMR Reloading `./server`...')
    const newApp = require('./server/server').default
    server.removeListener('request', currentApp)
    server.on('request', newApp)
    currentApp = newApp
  })
}
