# Megaversal


## Goals

Long term goal: one repo to build:

- [x] web
- [x] server (express)
- [ ] desktop (electron)
- [ ] desktop (windows 10 native)
- [ ] android (native)
- [ ] ios (native)

With:
- [x] typescript enabled, but optional
- [x] hmr enabled on all platforms (flaky, sometimes server needs to be restarted)
- [x] possibility for full test coverage with Jest
- [ ] good developer experience on
  - [x] Vscode
  - [ ] Vim
  - [ ] Atom

Showing state management through:
- [x] An example of pure react `setState`
- [ ] The Elm Architecture
- [ ] Reactive (mobX/Flyd/RxJs)

Showing examples of stores:
- [x] in memory server store
- [x] sqlite (without framework)
  - [ ] automated hydration of client store
- [ ] sqlite (with knex)
- [x] in memory client store (localStorage)
- [ ] Firebase

Showing communication through:
 - [x] regular HTTP
 - [x] Ajax (using `fetch`)
 - [ ] sockets

Styling:
- [x] per-module css loading
- [x] optionally Typestyle

Other Goodies:
- [x] React Router
- [x] TsLint

Examples:
- [x] Simple dumb component
- [x] Todo App
- [x] Stupidly simple crud app (blog)
- [ ] Authentication (passport?)
- [ ] Chat

## Quick Start

```bash
git clone <this repo>
cd <directory>
yarn start (or npm start)
```

Then open http://localhost:3000/ to see your app. Your console should look like this:

<img src="https://cloud.githubusercontent.com/assets/4060187/26324663/b31788c4-3f01-11e7-8e6f-ffa48533af54.png" width="500px" alt="Razzle Development Mode"/>

**Note**: Sometimes, `yarn start`, when run with an empty `build/` directory, fails to start the server. If you're running it for the first time, or if you deleted `build/`, you'll have to run `yarn start` twice.

**Note 2**: Server-side HMR doesn't work flawlessly. If you feel your changes aren't being picked up, do restart the server


## Interesting commands

Below is a list of commands you will probably find useful.

### `npm start` or `yarn start` 

Runs the project in development mode.   
You can view your application at `http://localhost:3000`

The page will reload if you make edits.

### `npm run build` or `yarn build`
Builds the app for production to the build folder.      

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

### `npm run start:prod` or `yarn start:prod`
Runs the compiled app in production.

You can again view your application at `http://localhost:3000`

### `npm test` or `yarn test`

Runs the test watcher (Jest) in an interactive mode.
By default, runs tests related to files changed since the last commit.


## Files

Description of what you'll find:

- `package.json`: sets the package name, which packages it depends on, a few commands, and Jest options
- `razzle.config.js`: this replaces the more classical `webpack.config.js`. Razzle aims to simplify webpack configuration, so it provides you with sane defaults, that you can override in this file
- `tsconfig.json`: typescript configuration
- `tslint.json`: tslint configuration. Tslint is a software that checks your files for style, and makes sure everyone on a project uses the same coding style
- `yarn.lock`: this is used by `yarn` when installing packages to store more data about those packages, so it can later download the exact same version you're using (in case there are subtle, undocumented changes between versions)