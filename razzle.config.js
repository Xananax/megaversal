'use strict';

module.exports = {
  modify( config, { target, dev }, webpack ) {
    config.resolve.extensions = config.resolve.extensions.concat([
      '.ts',
      '.tsx',
      '.md'
    ]);

    // Safely locate Babel-Loader in Razzle's webpack internals
    const babelLoader = config.module.rules.findIndex(
      rule => rule.options && rule.options.babelrc
    );

    // Get the correct `include` option, since that hasn't changed.
    // This tells Razzle which directories to transform.
    const { include } = config.module.rules[babelLoader];

    // Declare our TypeScript loader configuration
    const tsLoader = {
      include,
      test: /\.tsx?$/,
      loader: 'ts-loader',
      options: {
        // this will make errors clickable in `Problems` tab of VSCode
        visualStudioErrorFormat: true,
      },
    };


    const tslintLoader = {
      include,
      enforce: 'pre',
      test: /\.tsx?$/,
      loader: 'tslint-loader',
      options: {
        emitErrors: true,
        typeCheck:true,
        configFile: './tslint.json',
      },
    };

    // Fully replace babel-loader with ts-loader
    config.module.rules[babelLoader] = tsLoader;
    config.module.rules.push(tslintLoader);
    
    // If you want to use Babel & Typescript together (e.g. if you
    // are migrating incrementally and still need some Babel transforms)
    // then do the following:
    //
    // - COMMENT out line 45 (`...rules[babelLoader] = tsLoader)
    // - UNCOMMENT line 55 (... rules.push(tsLoader))
    //
    // config.module.rules.push(tsLoader)

    /**
     * Allow markdown loading
     */
    const markdownLoader = {
      test: /\.md$/,
      use: [
        {
          loader: "html-loader"
        },
        {
          loader: "markdown-loader",
        }
      ]
    }

    const fileLoaderIndex = config.module.rules.findIndex(
      rule => rule.exclude
    );

    config.module.rules[fileLoaderIndex].exclude.push(/\.md$/)
    config.module.rules.push(markdownLoader)

    /**
     * Allow for `__filename` and `__dirname` in
     * modules 
     */
    config.context = __dirname
    const node_conf ={ __filename: true,
      __dirname: true
    }
    
    config.node = {...config.node, ...node_conf}

    return config;
  },
};
